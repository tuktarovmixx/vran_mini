import os
import sys
import cv2
import json
import time
import requests
import argparse
import threading
import datetime
import numpy as np
import logging
import beanstalkc
import getopt
from multiprocessing.pool import ThreadPool
from multiprocessing import Process, Pipe
from openalpr import Alpr

pipe_front = "rtsp://"
pipeline1 = "rtsp://admin:Admin12345@10.20.30.148:554 latency=50 ! rtph264depay ! h264parse ! omxh264dec ! nvvidconv ! queue ! appsink"
pipe_back = ":554 latency=50 ! rtph264depay ! h264parse ! omxh264dec ! nvvidconv ! queue ! appsink"

Camera_Threads_Pool = ThreadPool(processes=1)
beanstalk = beanstalkc.Connection(host='localhost', port=11300)

def run(command):
	process = Popne(command, stdout=PIPE, shell=True)
	while True:
		line = process.stdout.readline().rstrip()
		if not line :
			break
		yield line

# muddabucking switch, lol
class switch(object):
	def __init__(self, value):
		self.value = value
		self.fall = False
	
	def __iter__(self):
		yield self.match
		raise StopIteration
	
	def match(self, *args):
		if self.fall or not args:
			return True
		elif self.value in args:
			self.fall = True
			return True
		else:
			return False
			
#creating new still image of found license plate
def Plate_Found_rectangle(cords, image):
	x = int(min(cords["coordinates"], key=lambda ev: ev["x"])["x"])
	y = int(min(cords["coordinates"], key=lambda ev: ev["y"])["y"])
	w = int(max(cords["coordinates"], key=lambda ev: ev["x"])["x"])
	h = int(max(cords["coordinates"], key=lambda ev: ev["y"])["y"])
	cv2.rectangle(image,(x,y),(w,h),(255,0,0),2)
	cv2.imshow("Plate Detected", image)
		
def ALPR_detect_at_image(alpr, image, camera_string):
	JSONdata = alpr.recognize_ndarray(image)
	if JSONdata["results"]:		
		max_confidence = max(JSONdata["results"], key=lambda ev: ev["confidence"])
		if int(max_confidence["confidence"]) > 85:
			print("License plate found at " + camera_string)
			print(max_confidence["plate"])
			print(max_confidence["confidence"])
			Plate_Found_rectangle(max_confidence, image)
			beanstalk.put(str(JSONdata["results"]))
			job = beanstalk.reserve()
			job.body
			job.delete()

#			print(JSONdata["results"])
			return 1
	else:
		return 0
#		else:
#			if cv2.getWindowProperty("Plate Detected",0) > 0:		
#				cv2.destroyWindow("Plate Detected")
		#for data in JSONdata["results"]:
			#print(data)

def Camera_Thread(datapipe, pipeline, width, height):
	print("...Camera init...")
	CameraCapture = cv2.VideoCapture(pipeline)	
	if CameraCapture.isOpened():
		print("...Camera initialized...")
		while True:
			ret, image = CameraCapture.read()
			imageSend = cv2.resize(image, (width, height))
			datapipe.send(imageSend)
	else:
		print ("...Cannot start video stream on camera. Stopping thread...")
		return -1
		
def main(argv):
	IP_address = ''
	try:
		opts, args = getopt.getopt(argv, "hi:o:",["ip_address="])
	except getopt.GetoptError:
		print 'vran_mini.py -ip <login:password@ip_address>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'vran_mini.py -ip <login:password@ip_address>'
		elif opt == '-i': 
			IP_address = arg
			print("...setting IP address as: " + IP_address)	
			pipeline1 = pipe_front + IP_address + pipe_back		

	print("...loading OpenALPR...")
	alpr = Alpr("eu", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data/")
	if not alpr.is_loaded():
		print("...Error loading OpenALPR...")
		sys.exit(1)
	alpr.set_top_n(2)
	print("...loading Beanstalk client...")	
	beanstalk.put('...VRAN_mini init succesfull...')
	job = beanstalk.reserve()
	job.body
	job.delete()
		
	cv2.namedWindow("Camera(s) View", cv2.WINDOW_AUTOSIZE)
	image_width = 320
	image_height = 240 
	image_width_REC = 1024
	image_height_REC = 768 
	# connect cam1
	datapipe1_parent, datapipe1_child = Pipe()	
	Camera1_Process = Process(target=Camera_Thread, args=(datapipe1_child, pipeline1, image_width_REC, image_height_REC))
	Camera1_Process.daemon = True
	Camera1_Process.start()
	#Camera1_Process.join()
	
	imageCam = np.zeros((image_width, image_height, 3), np.uint8)

	# cameras cycle	
	while cv2.getWindowProperty("Camera(s) View",0) >= 0:		
		# switch for rolling cameras
		if Camera1_Process.is_alive():
			image = datapipe1_parent.recv()
			imageCam = cv2.resize(image, (image_width, image_height))		 
		else:
			image = np.zeros((image_width, image_height, 3), np.uint8)
			imageCam = image
			print("...restarting camera...")
			# connect cam1
#			Camera1_Process.stop()			
			datapipe1_parent, datapipe1_child = Pipe()	
			Camera1_Process = Process(target=Camera_Thread, args=(datapipe1_child, pipeline1, image_width_REC, image_height_REC))
			Camera1_Process.daemon = True
			Camera1_Process.start()
		
#		cv2.waitKey(5)
		#imageALPR = cv2.resize(image, (image_width, image_height))		 
#		ALPR_detect_at_image(alpr, imageALPR, "Camera")
		ALPR_detect_at_image(alpr, image, "Camera")
		cv2.imshow("Camera(s) View", imageCam)
		#time.sleep(0.01)
												
		keyCode = cv2.waitKey(5) & 0xff
		if keyCode == 27 or cv2.getWindowProperty("Camera(s) View", 0) == -1:
			print("...Exiting script...")
			time.sleep(1)
			break

	print("...Closing video stream(s)...")				
	Camera1_Process.terminate()
	print("...Closing window(s)...")
	cv2.destroyAllWindows()
	exit()

if __name__ == "__main__":
	main(sys.argv[1:])
